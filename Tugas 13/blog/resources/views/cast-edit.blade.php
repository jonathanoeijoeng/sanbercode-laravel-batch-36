@extends('layouts.master')

@section('content')

<div class="container">
    <div class="card">
        <div class="card-header"><strong>Edit Data</strong></div>
        <div class="card-body">
            <form action="/cast/{{ $cast->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama.." value="{{ $cast->nama }}">
                    @error('nama')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="umur" class="form-label">Umur</label>
                    <input type="number" class="form-control" id="umur" name="umur" placeholder="Masukkan umur.." value="{{ $cast->umur }}">
                    @error('umur')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="bio" class="form-label">Biodata Diri</label>
                    <textarea class="form-control" id="bio" name="bio" rows="5">{{ $cast->bio }}</textarea>
                    @error('bio')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">UPDATE</button>
                <a type="button" href="/cast" class="btn btn-secondary">BACK</a>
            </form>
        </div>
    </div>
</div>

@endsection