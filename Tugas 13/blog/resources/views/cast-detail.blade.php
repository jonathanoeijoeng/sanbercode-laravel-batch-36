@extends('layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header"><strong>Tambah Data</strong></div>
        <div class="card-body">
            <!-- <form action="/cast" method="GET"> -->
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input value="{{ $cast->nama }}" type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama.." disabled>
                </div>
                <div class="mb-3">
                    <label for="umur" class="form-label">Umur</label>
                    <input value="{{ $cast->umur }}" type="number" class="form-control" id="umur" name="umur" placeholder="Masukkan umur.." disabled>
                </div>
                <div class="mb-3">
                    <label for="bio" class="form-label">Biodata Diri</label>
                    <textarea class="form-control" id="bio" name="bio" rows="5" disabled>{{ $cast->bio }}</textarea>
                </div>
                <a type="button" href="/cast" class="btn btn-primary">Back</a>
            <!-- </form> -->
        </div>
    </div>
</div>
@endsection