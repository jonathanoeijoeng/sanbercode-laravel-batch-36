@extends('layouts.master')

@section('header')
    <h3 class="font-weight-bold">Cast List</h3>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Cast List</div>
        <div class="card-body">
            <a class="btn btn-primary mb-3" href="/cast/create" role="button">
                Tambah Data
            </a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($casts as $cast)
                    <tr>
                        <td>{{ $cast->nama }}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a class="btn btn-primary mr-1" role="button" href="/cast/{{ $cast->id }}">Detail</a>
                                <a class="btn btn-secondary mr-1" role="button" href="/cast/{{ $cast->id }}/edit">Edit</a>
                                <form action="/cast/{{$cast->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" value="Delete" onclick="return confirm('Apakah anda yakin?');">
                                </form>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="text-center" colspan="2">No data</td>
                    </tr> 
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection