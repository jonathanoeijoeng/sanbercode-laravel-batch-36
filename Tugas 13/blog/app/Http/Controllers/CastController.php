<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('cast')->get();
        return view('cast', compact('casts'));    
    }

    public function create()
    {
        return view('cast-create');
    }

    public function store(Request $request) 
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer|max:100',
            'bio' => 'required|min:10'
        ],
        [
            'nama.required' => 'Nama tidak boleh kosong!',
            'umur.required'  => 'Umur tidak boleh kosong!',
            'bio.required'  => 'Biodata tidak boleh kosong!',
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);
        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('/cast-detail', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('/cast-edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer|max:100',
            'bio' => 'required|min:10'
        ],
        [
            'nama.required' => 'Nama tidak boleh kosong!',
            'umur.required'  => 'Umur tidak boleh kosong!',
            'bio.required'  => 'Biodata tidak boleh kosong!',
        ]);

        DB::table('cast')->where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete(); 
        return redirect('/cast');
    }

}
